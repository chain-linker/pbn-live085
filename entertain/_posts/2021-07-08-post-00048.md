---
layout: post
title: "해리포터 시리즈 순서(왓챠 추천 영화)_스포있음"
toc: true
---

 조앤 K. 롤링의 소설의 바탕인 호그와트 마법 학교에서 펼쳐지는 '해리포터'의 모험 시리즈는 전 인환 남녀노소 가리지 않고 큰 인기를 끈 영화이다.
 [왓챠]에서는 해리포터의 전 시리즈 8편을 공개했다. 노리장도 해리포터 시리즈를 엄청 좋아하는데 주말, 연말 집콕하면서 마법 학교의 모험을 함께 떠나볼까요?
 

 해리포터 시리즈 순서
 

 1. 해리포터와 마법사의 돌 (2001)
 

 11살 해리는 마법학교 호그와트의 교수이자 사냥터지기 해그리드에게서  입학 초대장을 받고 호그와트 특급열차를 탄다. 호그와트로 갈 이운 있는 9와 4분의 3 승강장에서 론을 만나고 열차에서 헤르미온느를 만나 친구가 된다. 이들은 4개의 숙사 그리핀도르, 후플푸프, 슬리데린, 레번클로에서 용감하고 대담한 자들을 위한 기숙사 그리핀도르에 배정받는다.

 호그와트에서 놀라운 모험의 세계를 경험하면서 마법을 배워 나가고 나르는 빗자루와 용머리가 세 가장귀 달린 개들을 무찌르고 [영원한 생을 가져다주는 마법사의 돌]을 알고  해리의 부모님을 죽인 [볼드모트]가 자기 돌을 노린다는 사실을 알게 된다. 해리는 론과 헤르미온느와 마법사의 돌, 또한 호그와트를 지키기 위해 싸운다.
 

 2. 해리포터와 비밀의 방 (2002)
 

 호그와트의 여름방학. 해리를 구박하는 이모 식구들과 있기에 즐겁지 않다. 방학이 끝나고 호그와트를 가는 열차를 놓치고 론의 윤서 [하늘을 나는 차]로 호그와트로 향한다.

 해리는 학교에서 뱀의 언어를 할 복 있는 능력과 학교에서 벌어지는 이상한 사건들, 돌처럼 굳어버리는 친구들 게다가 [비밀의 방]이 열렸다는 소문이 들려온다. [비밀의 방]에는 볼드모트가 어린 시기 남겨놓은 자신의 '기억' 톰 리들이 있었다. 톰 리들은 볼드모트 군 자체이며 저주받은 일기장에 그가 남겨있다.

 이놈 일기장은 읽은 사람을 조정하고 생명력을 빼앗아 볼드모트는 강해지는 물건이다. 뿐만 아니라 집요정 도비가 등장해 해리에 의해 자유의 몸이 된다. "Dobby Is free"
 

 3. 해리포터와 아즈카반의 죄수(2004)
 

 13세가 된 해리. 흔히 세상에서는 마법이 금지되어 있는데 자신의 부모를 험담하는 아저씨 누이에게 마법을 부리고 마법부의 징계가 무서워 도망치다가 '시리우스 블랙' 이익 아즈카반의 감옥에서 탈출했다는 소식을 듣는다.  시리우스 블랙은 해리포터의 목숨을 노리고 있었고, 아즈카반의 보수 [디멘터]가 호그와트에 머물면서 해리를 위협한다. 디멘터에게 위협을 당하고 있을 경우 그의 친구들과 어둠의 마법 방어술 훈육 루핀 교수의 마법으로 디멘터의 위협을 대처할 수명 있었다.

 시리우스 블랙과 루핀 교수의 정체가 밝혀지다! 해리의 부모를 죽이는데 가담한 줄 알았던 시리우스 블랙은 해리의 부모와 친구였고, 루핀 교인 더욱이 마찬가지였으며 심지어 시리우스 블랙은 해리의 대부였다. 진성 배신자였던 피터 페티그루를 찾으려는데 그는 론의 애완 쥐로 둔갑하며 살고 있었다.
 4. 해리포터와 천만의외 주잔 (2005)
 

 언제나 꾸는 악몽 그리하여 이마의 상처가 더욱 고통스럽다. 호그와트에선 마법사들 세계에서 부서 관념 있고 위험한 [트리위저드 대회]가 열리고 명문 3개 학교에서(영국_호그와트, 불가리아_덤스트랭, 프랑스_보바통) 선발된 한 명씩 출전해 목숨을 건 경연이 시작된다.

 몽외 잔에 의해 출전자를 선발하는데 누군가의 조작으로 인해 해리가 백장 참가하게 되어 그를 위험에 빠뜨린다. 재차 부임한 오러 입신 [매드아이 무디]는 해리가 대회에 참가하도록 돕는다.

 목숨을 건 대회가 열리고, 첨단 관문에서 세드릭과 해리는 공동묘지로 순간이동을 하게 되는데 볼드모트가 등장한다. 트리위저드 컵은 해리를 볼드모트에게 보내기 위한 함정이었고, 볼드모트 아버지의 뼈, 웜테일의 살과 해리의 피로 그는 부활한다. 그곳에서 세드릭은 볼드모트에게 죽임을 당한다.
 

 5. 해리포터와 불사조 기사단 (2007)
 

 호그와트 최대의 위기! 천천만의외 여영 편에서 볼드모트가 부활하고 , 머글(마법사가 아닌 일반인)에게 마법을 사용한 일로 해리는 퇴학이 될 거라는 편지를 받는다. [불사조의 기사단]은 14년 전, 볼드모트와 그의 추종자들을 대항하기 위해 만든 엄비 단체이며 해리 부모님, 시리우스 블랙, 스네이프, 루핀, 위즐리 부부가 일원이다.

 해리의 퇴학 청문회가 열리고 덤블도어 교수는 해리가 무죄를  받도록 도와준다. 새로운 어둠의 [이 글](https://snap-haircut.com/entertain/post-00003.html){:rel="nofollow"} 마법 방어술 교수이자 마법부의 비서실장 [엄브릿지]가 등장하며 호그와트에서 권력을 휘두르고, 해리는 몇몇의 친구들과 [덤블도어 군대] 어둠의 마법 방어술 육영 모임을 만든다. 해리는 악몽을 꾸고 해리와 볼드모트의 정신이 연결되어 있다는 점을 알게 된다.

 볼드모트 계략에 넘어간 해리와 친구들이 위험에 처하고 그의 추종자 [벨라 트릭스]가 시리우스에게  '아바다 케다브라' 마법을 걸어 죽고 만다. 더구나 볼드모트가 살아 있음을 모두가 알게 된다.
 

 6. 해리포터와 혼혈왕자(2009)
 

 후머리 전투를 준비하기 위해 노력하는 해리와 친구들. 어둠의 여력 볼드모트의 힘이 더욱 강해져 머글 세계와 호그와트까지 위험한 가운데, 볼드모트를 물리칠 행우 있는 그의 영혼을 나눈 7개의 [호크룩스]를 파괴해야만 한다.

 뿐만 아니라 혼혈왕자의 책이 등장! 해리는 혼혈왕자 책에서 보던 [섹튬셈프라] 주문을 말포이에게 건다. 크게 다친 말포이를 보자 책을 막 없애기로 하고 소득 책을 찾지 못하도록 [필요의 방]에 숨기기로 한다.

 말포이는 [죽음을 먹는 자]들을 호그와트로 소환하고 말포이 대체 스네이프 교수는 덤블도어 교수에게 마법을 걸어 죽인다. 뿐만 아니라 스네이프 교수는 [내가 곧장 '혼혈왕자'다]라고 말한다.
 

 7. 해리포터와 죽음의 성물 (2010~2011)
 

 Ⅰ. 덤블도어 교수의 몰 이후, 마법부와 호그와트는 볼드모트와 죽음을 먹는 자들에게 점령당한다. 불사조의 기사단이 다시모이고, 은신처로 가려하는데 죽음을 먹는 자들이 공격하여 기사단 안에 내부 배신자가 있다고 생각하며 호상간 경계한다. 이 과정에서 조지는 부상을 입고 매드아이는 죽음을 당한다.

 마법부 장관이 찾아와 론과 헤르미온느, 해리에게 덤블도어의 유언과 그들에게 남긴 물건을 준다. 론에게는"세상이 어둠으로 뒤덮였을 물정 빛을 보여 줄 것이다" 라며 [딜루미 네이터]를 남기고, [방랑 총환 비들의 이야기] 장서 한량 권, 재미와 교육을 얻길 바라며 헤르미온느에게 책을 남겼고, 해리에게는 인내와 기술에 대한 보상을 상기시키는 용모 [첫 퀴디치 경기에서 잡은 스니치]와 [고드릭 그리핀도르의 칼]을 남겼다.

 그들은 호크룩스를 파괴하려 긴 여정을 떠나고, 과정에서 도비는 해리를 구하려다가 죽음을 당한다. 볼드모트는 덤블도어 무덤에서 죽음의 성물 한가운데 하나인 [딱총나무 지팡이]를 얻는다.
 Ⅱ. 해리는 결국 퍼즐을 완성하고 덤블도어와 어둠의 마법사 [그린델왈드]에 알게 된다. 볼드모트는 해리에 의해 호크룩스들이 파괴되었음을 느끼고 호그와트로 향한다. 볼드모트는 해리를 넘기면 나머지는 무사하며, 호그와트를 건들지 않겠다고 말한다. 덤블도어의 지팡이가 말을 요조숙녀 듣자 볼드모트는 덤블도어를 죽인 스네이프를 죽인다.

 스네이프는 해리에게 자신의 눈물을 담아 [펜 시브]에 가져가라 말하고 숨을 거둔다. 죽음의 먹는 자들과의 싸움에서 프레드와 루핀, 통스가 죽은 것을 알고 슬퍼한다. 펜 시브를 통해 스네이프의 기억을 보게 되고 스네이프와 덤블도어 교수가 자신을 끔찍하게 지켜왔는지를 보게 된다. 더욱이 시마이 호크룩스가 [내기니 :볼드모트와 으레 같이 다니는 뱀]라는 것을 알고 볼드모트에게 향하고, 각자의 자리에서 위험한 전투가 시작된다.

 

 무려 11년 가항 해리포터는 우리와 함께했다. 마법 판타지 영화라니? 원작인 소설도 정말로 재미있었지만 영화 더욱이 상상 만들어서 장구히 지난 지금에도 찾아보는 영화인 해리포터 시리즈.
 

 다시 봐도 재미있는 영화 [해리포터 시리즈] 왓챠에서 정주행 하세요. ( •̀.̫ •́ )✧
 

 "always"
 [죽음의 성물 중에서 세베루스 스네이프 교수의 대사. 故 앨런 릭먼 ]
